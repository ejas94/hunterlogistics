$(document).ready(function functionName() {
    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 69)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // spy
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 70
    });

    // cierro el menu
    $('.navbar-collapse ul li a:not(.dropdown-toggle)').click(function() {
        $('.navbar-toggle:visible').click();
    });

    // Offset para el nav
    $('#mainNav').affix({
        offset: {
            top: 70
        }
    })

    // Cerrando los paneles
    $('#firstLine, #secondLine, #thirdLine, #fourLine').on('show.bs.collapse', '.collapse', function() {
        $('#firstLine, #secondLine, #thirdLine, #fourLine').find('.collapse.in').collapse('hide');
    });
})
