<!DOCTYPE html>
<html lang="en">

<head>
    <title>Hunter Logistics</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <!-- Fonts -->
    <link href="css/fonts.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css">
</head>

<body id="page-top">
    <!-- Header -->
    <header>
        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Hunter Logistics</span><i class="fa fa-bars"></i>
                    </button>
                    <a class="page-scroll visible-xs visible-sm" href="#page-top"><img class="navbar-brand" src="img/hunterlogistics_logo_white.png" /></a>
                    <figure class="logo-menu">
                        <a id="logo-blanco" class="page-scroll" href="#page-top">
                            <img class="hidden-xs hidden-sm" src="img/hunterlogistics_logo_white.png" />
                        </a>
                    </figure>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="height: 70px !important">
                    <ul class="nav navbar-nav navbar-right" style="padding-top: 10px">
                        <li>
                            <a class="page-scroll" href="#about">About us</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#services">Services</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#ourTeam">Our team</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#productsWeMake">Products we make</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#checkCriteria">Check criteria</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#contact">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Logo central header -->
        <div class="header-content">
            <div class="header-content-inner">
                  <img src="img/hunterLogistics_Logo.svg" alt="logo"/>
            </div>
        </div>
    </header>
    <!-- Section del about -->
    <section class="bg-primary sector" id="about">
        <div class="container">
            <div class="">
                <div class="col-xs-12 text-center">
                    <h2 class="titulo-sector izquierda">About <span class="underline">us</span></h2>
                    <p>
                        At Hunter Logistic Limited, we’re here to provide a cost effective solution for the procurement, processing, and delivery of your orders, while ensuring the service and quality your customers have come to expect. You’ll find that our services will cost
                        far less than any in-house solution you may be currently utilising.
                        <br>
                        <br> Leveraging the rich knowledge and industry experience of over 500 specialists in our global team, the Hunter Logistic organisation is dedicated to helping you meet the Quality, Safety and Ethical Standards you demand throughout
                        your supply chain; thereby improving your competitiveness and efficiency in the global marketplace.
                    </p>
                </div>
            </div>
        </div>
        <!-- Imagen about -->
        <figure class="container-fluid pad0 img-sector img-1">
            <!-- <img src="img/about_us.jpg" alt="About us" class="img-responsive" /> -->
        </figure>
        <div class="la-raya"></div>
        <hr>
    </section>

    <!-- Section del services -->
    <section class="bg-primary sector" id="services">
        <div class="container">
            <div class="">
                <div class="col-xs-12 text-center">
                    <h2 class="titulo-sector izquierda"><span class="underline">Services</span></h2>
                    <p class="mb50">
                        Quality and success are directly proportional. As quality declines, costs increase and you lose business. That is why you need a partner you can trust, at a price you can afford. HUNTER LOGISTIC understands that our success depends on your success, and
                        that dependable inspection services are the cornerstone of quality deliverables. By inspecting products at the source, during production, prior to shipment, and before container sealing, the cost and risk associated with delays,
                        poor quality, and product rejections are significantly reduced. The guiding principles of quality inspections is simple: the early detection of defects, quick resolution, and support of on-time delivery.
                    </p>
                    <!-- Acordeon -->
                    <!-- Linea superior -->
                    <div class="col-xs-12 pad0" id="firstLine">
                        <!-- izquierdo -->
                        <div class="col-xs-4 pad0 izquierda">
                            <h5><a class="collapsed" data-toggle="collapse" href="#collapse1-1"><i class="fa" aria-hidden="true"></i> Search of suppliers</a></h5>
                        </div>
                        <!-- medio -->
                        <div class="col-xs-4 pad0 centrado">
                            <h5> - Procurement from Manufacturer</h5>
                        </div>
                        <!-- derecho -->
                        <div class="col-xs-4 pad0 derecha">
                            <h5><a class="collapsed" data-toggle="collapse" href="#collapse1-2"><i class="fa" aria-hidden="true"></i> Pre-production Inspection (PPI)</a></h5>
                        </div>
                        <!-- Texto linea superior -->
                        <div class="col-xs-12 pad0 mt20">
                            <!-- texto del izquierdo -->
                            <div class="collapse" id="collapse1-1">
                                <div class="card card-block">
                                    <h3>Search of suppliers</h3>
                                    <p>
                                        We are experts to find the best suppliers with best prices in the market. We currently have a list of more than 1,000 certified suppliers in different areas (lighting equipment, machinery, electronics and appliances, building materials).
                                    </p>
                                </div>
                            </div>
                            <!-- texto del derecho -->
                            <div class="collapse" id="collapse1-2">
                                <div class="card card-block">
                                    <h3>Pre-production Inspection (PPI)</h3>
                                    <p>
                                        The inspection process starts with a thorough assessment of raw materials, accessories, components, semi-finished and finished samples against client's specifications and/or reference samples, to predict potential product problems and recommend resolutions.
                                        The PPI gives you confidence that your product specifications are clearly understood and are being complied with before production begins.
                                    </p>
                                    <h4>THE PPI INCLUDES:</h4>
                                    <p>
                                        • &nbsp;&nbsp;&nbsp; Factory's production lines & capability.
                                        <br> • &nbsp;&nbsp;&nbsp; Factory's facilities & equipment.
                                        <br> • &nbsp;&nbsp;&nbsp; Raw materials main components & accessories.
                                        <br> • &nbsp;&nbsp;&nbsp; Semi-finished samples.
                                        <br> • &nbsp;&nbsp;&nbsp; Some finished samples.
                                        <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Linea Segunda -->
                    <div class="col-xs-12 pad0" id="secondLine">
                        <!-- izquierdo -->
                        <div class="col-xs-4 pad0-15 izquierda">
                            <h5><a class="collapsed" data-toggle="collapse" href="#collapse2-1"><i class="fa" aria-hidden="true"></i> During Production Inspection (DPI)</a></h5>
                        </div>
                        <!-- medio -->
                        <div class="col-xs-4 centrado">
                            <h5><a class="collapsed" data-toggle="collapse" href="#collapse2-2"><i class="fa" aria-hidden="true"></i> Pre-shipment Inspection (PSI)</a></h5>
                        </div>
                        <!-- derecho -->
                        <div class="col-xs-4 pad15-0 derecha">
                            <h5><a class="collapsed" data-toggle="collapse" href="#collapse2-3"><i class="fa" aria-hidden="true"></i> Sorting Inspection (SI)</a></h5>
                        </div>
                        <!-- Texto linea segunda -->
                        <div class="col-xs-12 pad0 mt20">
                            <!-- texto del izquierdo -->
                            <div class="collapse" id="collapse2-1">
                                <div class="card card-block">
                                    <h3>During Production Inspection (DPI)</h3>
                                    <p>
                                        When 20%-30% of goods have been completed, we inspect and evaluate the quality of semi-finished and finished items against your specifications. The DPI enables you to confirm that quality, as well as compliance to specifications, is being maintained throughout
                                        the production process. It also provides early detection of any issues requiring correction, thereby reducing delays.
                                    </p>
                                    <h4>THE DPI INCLUDES:</h4>
                                    <p>
                                        • &nbsp;&nbsp;&nbsp; The production status during the manufacturing process.
                                        <br> • &nbsp;&nbsp;&nbsp; Production line evaluation and timeline verification.
                                        <br> • &nbsp;&nbsp;&nbsp; Random sampling of semi-finished and finished products.
                                        <br> • &nbsp;&nbsp;&nbsp; Verify package and packaging material details.
                                        <br> • &nbsp;&nbsp;&nbsp; Overall assessment and recommendations.
                                        <br>
                                    </p>
                                </div>
                            </div>
                            <!-- texto del medio -->
                            <div class="collapse" id="collapse2-2">
                                <div class="card card-block">
                                    <h3>Pre-shipment Inspection (PSI)</h3>
                                    <p>
                                        A Pre-shipment Inspection is the inspection performed when goods are 100% completed, packed and ready for shipment. Our inspectors select random samples from finished goods according to the international statistical standard known as MIL-STD-105E (IS02859-1).
                                        The PSI confirms that finished products are in full compliance with your specifications.
                                    </p>
                                    <h4>THE PSI INCLUDES:</h4>
                                    <p>
                                        • &nbsp;&nbsp;&nbsp; Quantity verification.
                                        <br> • &nbsp;&nbsp;&nbsp; Style and color.
                                        <br> • &nbsp;&nbsp;&nbsp; Workmanship (general appearance).
                                        <br> • &nbsp;&nbsp;&nbsp; Functions and safety.
                                        <br> • &nbsp;&nbsp;&nbsp; Size specification, if required.
                                        <br> • &nbsp;&nbsp;&nbsp; Package details.
                                        <br> • &nbsp;&nbsp;&nbsp; Shipping marks.
                                        <br>
                                    </p>
                                </div>
                            </div>
                            <!-- texto del derecho -->
                            <div class="collapse" id="collapse2-3">
                                <div class="card card-block">
                                    <h3>Sorting Inspection (SI)</h3>
                                    <p>
                                        A Sorting Inspection is carried out pre or post packaging. A piece by piece inspection is performed on every item to evaluate general appearance, workmanship, function, safety and etc. as specified by you. The SI allows for the removal of all defective products from a shipment. Upon completion, all products which pass inspection are then sealed with HUNTER LOGISTIC stickers. This ensures that every piece included in the shipment meets your specified quality requirements.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Linea Tercera -->
                    <div class="col-xs-12 pad0" id="thirdLine">
                        <!-- izquierdo -->
                        <div class="col-xs-4 pad0-15 izquierda">
                            <h5> - Sample Picking</h5>
                        </div>
                        <!-- medio -->
                        <div class="col-xs-4 centrado">
                            <h5><a class="collapsed" data-toggle="collapse" href="#collapse3-2"><i class="fa" aria-hidden="true"></i> Loading Supervision</a></h5>
                        </div>
                        <!-- derecho -->
                        <div class="col-xs-4 pad15-0 derecha">
                            <h5> - Factory Visit Escort</h5>
                        </div>
                        <!-- Texto linea Tercera -->
                        <div class="col-xs-12 pad0 mt20">
                            <!-- texto del medio -->
                            <div class="collapse" id="collapse3-2">
                                <div class="card card-block">
                                    <h2>Loading Supervision</h2>
                                    <p>
                                        Wherever loading takes place, our inspectors will monitor the entire process. This includes a condition evaluation of the shipping container, verification of product information, quantities, packaging, and overall supervision of the loading process. Once the container is loaded, an HUNTER LOGISTIC seal is applied to the container to reduce the high risk of product substitution after loading.
                                    </p>
                                    <h4>PROCEDURE:</h4>
                                    <p>
                                      • &nbsp;&nbsp;&nbsp;	Record the weather/arrival time of container/container No./truck No. <br>
                                      • &nbsp;&nbsp;&nbsp;	Check the inner and outer condition of container to assess damage, wetness, perforation, smell <br>
                                      • &nbsp;&nbsp;&nbsp;	Check the quantity of goods loaded and record the condition of outer packaging (master cartons/pallets)  <br>
                                      • &nbsp;&nbsp;&nbsp;	Randomly select and open sample cartons to verify compliance with customer's specifications <br>
                                      • &nbsp;&nbsp;&nbsp;	Supervise the loading process to minimise breakage and maximise space utilisation <br>
                                      • &nbsp;&nbsp;&nbsp;	Seal the container with the customs' seal and HUNTER LOGISTIC seal <br>
                                      • &nbsp;&nbsp;&nbsp;	Record the seal Nos. and departure time of containers <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Linea cuarta -->
                    <div class="col-xs-12 pad0" id="fourLine">
                        <!-- izquierdo -->
                        <div class="col-xs-4 pad0-15 izquierda">
                            <h5> - Metal Detection</h5>
                        </div>
                        <!-- medio -->
                        <div class="col-xs-4 centrado">
                            <h5> - Product Spec. Making</h5>
                        </div>
                        <!-- derecho -->
                        <div class="col-xs-4 pad15-0 derecha">
                            <h5> - Customised Quality Program</h5>
                        </div>
                    </div>

                    <!-- Fin del acordeon -->
                    <div style="height:360px"></div>
                </div>
            </div>
        </div>
        <hr>
    </section>

    <!-- Section del our team -->
    <section class="bg-primary sector" id="ourTeam">
        <div class="container">
            <div class="">
                <div class="col-xs-12 text-center">
                    <h2 class="titulo-sector izquierda">Our <span class="underline">Team</span></h2>
                    <p>
                        At present, HUNTER LOGISTIC professionally trained, with rich experience and international view.
                        <br>
                        <br> HUNTER LOGISTIC has a large team of experts, covering petroleum, chemical and mineral products, chemical products, agricultural products, industrial products, consumer products, food, automobile, logistics, etc.
                    </p>
                </div>
            </div>
        </div>
        <!-- imagen del our team -->
        <figure class="container-fluid pad0 img-sector img-2 mb50">
            <!-- <img src="img/our_team.jpg" alt="About us" class="img-responsive" /> -->
        </figure>
        <div class="la-raya"></div>
        <hr>
    </section>

    <!-- Section del products we make -->
    <section class="bg-primary sector" id="productsWeMake">
        <div class="container">
            <div class="">
                <div class="col-xs-12">
                    <h2 class="titulo-sector izquierda">Products we <span class="underline">make</span></h2>
                    <!-- Primera fila -->
                    <div class="col-xs-12 pad0">
                        <div class="col-xs-4 pad0 izquierda">
                            <h5>
                              - Electronic & Household
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 centrado">
                            <h5>
                              - Electrical Appliances
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 derecha">
                            <h5>
                              - Lighting Equipment
                            </h5>
                        </div>
                    </div>
                    <!-- Segunda fila -->
                    <div class="col-xs-12 pad0">
                        <div class="col-xs-4 pad0 izquierda">
                            <h5>
                              - Vehicles Spare Parts
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 centrado">
                            <h5>
                              - Machinery
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 derecha">
                            <h5>
                              - Hardware & Tools
                            </h5>
                        </div>
                    </div>
                    <!-- Tercera fila -->
                    <div class="col-xs-12 pad0">
                        <div class="col-xs-4 pad0 izquierda">
                            <h5>
                              - Building Materials
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 centrado">
                            <h5>
                              - Consume Goods
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 derecha">
                            <h5>
                              - Gifts
                            </h5>
                        </div>
                    </div>
                    <!-- Cuarta fila -->
                    <div class="col-xs-12 pad0">
                        <div class="col-xs-4 pad0 izquierda">
                            <h5>
                              - Home Decorations
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 centrado">
                            <h5>
                              - Textiles & Garments
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 derecha">
                            <h5>
                              - Shoes
                            </h5>
                        </div>
                    </div>
                    <!-- Quinta fila -->
                    <div class="col-xs-12 pad0">
                        <div class="col-xs-4 pad0 izquierda">
                            <h5>
                              - Office Supplies
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 centrado">
                            <h5>
                              - Cases & Bags
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 derecha">
                            <h5>
                              - Recreation Products
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- imagen del products we make -->
        <figure class="container-fluid pad0 img-sector img-3">
            <!-- <img src="img/products_we_make.jpg" alt="About us" class="img-responsive" /> -->
        </figure>
        <div class="la-raya"></div>
        <hr>
    </section>

    <!-- Section del ckechs criteria -->
    <section class="bg-primary sector" id="checkCriteria">
        <div class="container">
            <div class="">
                <div class="col-xs-12 text-center">
                    <h2 class="titulo-sector izquierda">Checking <span class="underline">criteria</span></h2>
                    <!-- Primera fila -->
                    <div class="col-xs-12 pad0">
                        <div class="col-xs-4 pad0 izquierda">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Quantity status
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 centrado">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Product appearance
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 derecha">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Performance & function
                            </h5>
                        </div>
                    </div>
                    <!-- Segunda fila -->
                    <div class="col-xs-12 pad0">
                        <div class="col-xs-4 pad0 izquierda">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Workmanship
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 centrado">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Assembly
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 derecha">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Accessories
                            </h5>
                        </div>
                    </div>
                    <!-- Tercera fila -->
                    <div class="col-xs-12 pad0">
                        <div class="col-xs-4 pad0 izquierda">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Material
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 centrado">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Colours
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 derecha">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Logo
                            </h5>
                        </div>
                    </div>
                    <!-- Cuarta fila -->
                    <div class="col-xs-12 pad0">
                        <div class="col-xs-4 pad0 izquierda">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Size & measurements
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 centrado">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Weight
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 derecha">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Assortment
                            </h5>
                        </div>
                    </div>
                    <!-- Quinta fila -->
                    <div class="col-xs-12 pad0">
                        <div class="col-xs-4 pad0 izquierda">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Carton Status
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 centrado">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Barcode scan test
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 derecha">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Packing & Marking
                            </h5>
                        </div>
                    </div>
                    <!-- Sexta fila -->
                    <div class="col-xs-12 pad0">
                        <div class="col-xs-4 pad0 izquierda">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Shipping marks
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 centrado">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Factory view
                            </h5>
                        </div>
                        <div class="col-xs-4 pad0 derecha">
                            <h5>
                                <i class="fa fa-check-circle-o" aria-hidden="true"> </i> Container inspection
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- imagen del checks criteria -->
        <figure class="container-fluid pad0 img-sector img-4">
            <!-- <img src="img/chech_criteria.jpg" alt="About us" class="img-responsive" /> -->
        </figure>
        <div class="la-raya"></div>
        <hr style="margin-bottom: 100px;">
    </section>

    <!-- Section del contacto -->
    <section class="bg-primary sector" id="contact" style="padding:0 !important;">
        <div class="container-fluid pad0">
            <div class="contacto">
                <div class="container">
                    <div class="col-xs-12 col-md-6 mt50">
                        <div class="col-xs-12">
                            <h2 class="titulo-sector izquierda" style="color:white">Contact <span class="underline">us</span></h2>
                            <form id="formulario-contacto" class="form-horizontal col-xs-12 pad0" method="post">
                                <ul class="col-xs-12 pad0">
                                    <!-- Nombre -->
                                    <li class="">
                                        <label for="nombre" class="sr-only ">Name:</label>
                                        <input class="" name="entradas" type="text" id="nombre" placeholder="Name">
                                    </li>
                                    <!-- Apellido -->
                                    <li class="">
                                        <label for="apellido" class="sr-only ">Email:</label>
                                        <input class="" name="entradas" type="text" id="apellido" placeholder="Email">
                                    </li>
                                    <!-- E-mail -->
                                    <li class="">
                                        <label for="email" class="sr-only ">Phone:</label>
                                        <input class="" name="entradas" type="text" id="email" placeholder="Phone">
                                    </li>
                                    <!-- consulta -->
                                    <li class="">
                                        <label for="telefono" class="sr-only ">Message:</label>
                                        <textarea class="" type="textarea" name="entradas" id="consulta" placeholder="Message" rows="8" cols="40"></textarea>
                                    </li>
                                </ul>
                                <button type="button" class="col-xs-2 pull-right pad0">SEND</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 hidden-xs hidden-sm">
                        <figure>
                            <img src="img/hunterlogistics_logo_white.png" alt="logo" />
                        </figure>
                    </div>
                    <div class="direccion col-xs-12">
                        <div class="col-xs-12 col-sm-6 pad0">
                            <p>
                                <i class="fa fa-map-marker"></i> Flat/R, T26 G/F, 18 Bonham Strand, West Sheung Wan. Hong Kong
                            </p>
                        </div>
                        <div class="col-xs-12 col-sm-6 pad0">
                            <p class="col-xs-6">
                                <i class="fa fa-phone" aria-hidden="true"></i> (852) 27851778 / (852) 36941778
                            </p>
                            <p class="col-xs-6">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i> contact@hunterlogisticslimited.com
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pad20">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1845.902554567424!2d114.15126046795616!3d22.28537700520414!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3404007c5e9eb467%3A0x226691e9a5e7edba!2s18+Bonham+Strand%2C+Sheung+Wan%2C+Hong+Kong!5e0!3m2!1ses-419!2sve!4v1468101500914"
                    width="100%" height="360px" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </section>

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/main.js"></script>

</body>

</html>
